*forked from [OCCRP webschlocker](https://git.occrp.org/libre/webschlocker3) to update ruby version*

# [schleuder-web](https://0xacab.org/schleuder/schleuder-web) on docker

This repo contains a docker configuration for "schleuder-web", a web interface for an encrypted group email system "schleuder".

See:

* https://0xacab.org/schleuder/schleuder
* https://0xacab.org/schleuder/schleuder-web
* https://0xacab.org/schleuder/schleuder-cli

If a valid database is not found, `rake db:setup` is run inside the container to set-up a basic valid database.

## Communication with "schleuder-api-daemon"

This image requires a "schleuder-api-daemon" running somewhere and accessible via TCP/IP -- one option is to run the ["schlocker" docker image](https://0xacab.org/schleuder/schlocker). You can configure the "schleuder-api-daemon" host with the `WEBSCHLOCKER_CONFIG_API_HOST` environment variable described below.


## Running and testing

You need `docker`, obviously. So first, [go and install it](https://docs.docker.com/engine/installation/). Then get "schleuder-api-daemon" running. We'll assume you're [using the "schlocker" docker image](https://0xacab.org/schleuder/schlocker/#running-and-testing) for this. Once you have "schlocker" running, run "schlocker-web":

```
$ git clone https://0xacab.org/schleuder/schlocker-web.git
$ docker build -t 'schlocker-web' schlocker-web
$ docker run --rm --name schlocker-web-test --link schlocker-test -e WEBSCHLOCKER_CONFIG_API_HOST="schlocker-test" -e WEBSCHLOCKER_CONFIG_TLS_FINGERPRINT="fingerprint_from_schlocker_output" -e WEBSCHLOCKER_CONFIG_API_KEY="api_key_given_also_to_schlocker" schlocker-web
```

By default "schleuder-web" listens on port 3000 (you can change this by setting `$WEBSCHLOCKER_BIND_PORT` docker envvar) and will inform you about the IP address it is running on in the output.

Once you have that, navigate your browser to http://<IP-of-the-container>:3000/ and log-in with user `root@localhost`, password `slingit!`.

## Environment variables

 - `WEBSCHLOCKER_CONFIG_HOSTNAME` (default: container's hostname)

The hostname "schleuder-web" will run under, used among others in confirmation links sent to users.

 - `WEBSCHLOCKER_BIND_ADDRESS` (default: `0.0.0.0`)
 - `WEBSCHLOCKER_BIND_PORT` (default: `3000`)
 
Hostname (or IP address) and port to bind to.

 - `WEBSCHLOCKER_CONFIG_API_HOST` (default: `localhost`)

Host the "schleuder-api-daemon" can be reached at.

 - `WEBSCHLOCKER_CONFIG_API_PORT` (default: `4443`)

Port the "schleuder-api-daemon" can be reached at.
 
 - `WEBSCHLOCKER_CONFIG_MAILER_FROM` (default: `noreply@$WEBSCHLOCKER_CONFIG_HOSTNAME`)
 
Sender address for all the e-mails originating from the web interface (i.e. confirmation e-mails). Keep in mind that this should be an address that the e-mail server will let through.
 
 - `WEBSCHLOCKER_CONFIG_DELIVERY_METHOD` (default: `smtp`)
 
Delivery method to use for outgoing e-mail; "schleuder-web" uses [`ActionMailer`](http://api.rubyonrails.org/classes/ActionMailer/Base.html) to send mail.
 
 - `WEBSCHLOCKER_CONFIG_SENDMAIL_ARGUMENTS` (default: `-i`)
 
Arguments passed to `sendmail`, if `WEBSCHLOCKER_CONFIG_DELIVERY_METHOD` is set to `sendmail`.
 
 - `WEBSCHLOCKER_CONFIG_SMTP_ADDRESS` (default: `localhost`)
 - `WEBSCHLOCKER_CONFIG_SMTP_PORT` (default: `25`)
 
SMTP server address and port to be used when `WEBSCHLOCKER_CONFIG_DELIVERY_METHOD` is set to `smtp`.
 
 - `WEBSCHLOCKER_CONFIG_SMTP_OPENSSL_VERIFY_MODE` (default: `none`)
 
How should the server cert be verified, if at all, when `WEBSCHLOCKER_CONFIG_DELIVERY_METHOD` is set to `smtp`. Currently not used at all.

 - `WEBSCHLOCKER_SECRET_KEY_BASE` (default: generated random string)
 
Secret used to verify encrypted cookies; can be changed at any time (change causes cookies to become invalid; users are then required to re-login).

### Database settings

Separate databases are used by "schleuder" and "schleuder-web"; these settings should thus be different from the ones used for "[schlocker](https://git.occrp.org/libre/schlocker3/)".

 - `WEBSCHLOCKER_DB_ADAPTER` (default: `sqlite3`)

Database adapter.
 
 - `WEBSCHLOCKER_DB_DATABASE` (default: `/var/schleuder-web/db.sqlite`)
 
Database name (or database file path when using `sqlite3` adapter).
 
 - `WEBSCHLOCKER_DB_ENCODING`

Database encoding (not used for `sqlite3`).

 - `WEBSCHLOCKER_DB_USERNAME`

Database user (not used for `sqlite3`).

 - `WEBSCHLOCKER_DB_PASSWORD`

Database user password (not used for `sqlite3`).

 - `WEBSCHLOCKER_DB_HOST`
 
Database host (not used for `sqlite3`).


## TODO

 - handle more [`ActionMailer` config options](http://api.rubyonrails.org/classes/ActionMailer/Base.html)
