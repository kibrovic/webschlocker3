FROM ruby:2.5
MAINTAINER Friedrich Lindenberg <pudo@occrp.org>, Michał Woźniak <rysiek@occrp.org>
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y \
    libmagic-dev \
    libgpgme11-dev \
    wget \
    git \
    git-core \
    --no-install-recommends && rm -rf /var/lib/apt/lists/*
    
RUN git clone https://0xacab.org/schleuder/schleuder-web.git /opt/schleuder-web && \
    cd /opt/schleuder-web && \
    bin/setup
    
# remove the default config files
RUN rm /opt/schleuder-web/config/schleuder-web.yml /opt/schleuder-web/config/secrets.yml /opt/schleuder-web/config/database.yml

COPY entrypoint.sh /sbin/entrypoint.sh
RUN chmod a+x /sbin/entrypoint.sh

WORKDIR /opt/schleuder-web 
EXPOSE 3000
ENTRYPOINT ["/sbin/entrypoint.sh"]
CMD ["bundle", "exec", "rails", "server", "-b", "$WEBSCHLOCKER_BIND_ADDRESS", "-p", "$WEBSCHLOCKER_BIND_PORT", "-e", "production"]
